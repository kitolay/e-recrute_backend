require('dotenv/config');

module.exports = {
  // HOST: "127.4.188.2",
  HOST: process.env.HOSTDB || "localhost",
  USER: process.env.USERDB || "talentam_db",
  PASSWORD: process.env.PASSWORDDB || "P@ssw0rd",
  DB: process.env.DB || "talentam_db",
  dialect: "mysql",
  PORT: "3306",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
